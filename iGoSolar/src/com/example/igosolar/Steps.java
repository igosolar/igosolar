package com.example.igosolar;

import com.example.igosolar.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class Steps extends Activity {
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = true;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = true;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;

	/**
	 * This will set configure the options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_activity_actions, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	/**
	 * This set the options menu actions
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_headerwhygosolar:
	        	Intent intentwgs = new Intent(this, WhyGoSolar.class);
	        	startActivity(intentwgs);
	            return true;
	        case R.id.action_headergpslocation:
	        	Intent intentgps = new Intent(this, LocationServices.class);
	        	startActivity(intentgps);
	            return true;
	        case R.id.action_headersteps:
	        	Intent intentsteps = new Intent(this, Steps.class);
	        	startActivity(intentsteps);
	            return true;
	        case R.id.action_headersolarevents:
	        	Intent intentevents = new Intent(this, Events.class);
	        	startActivity(intentevents);
	            return true;
	        case R.id.action_mainmenu:
	        	Intent intentmain = new Intent(this, Main.class);
	        	startActivity(intentmain);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_steps);
		
		TextView tv = (TextView) findViewById(R.id.stepscontent);
		tv.setMovementMethod(LinkMovementMethod.getInstance());
		tv.setText(Html.fromHtml(getResources().getString(R.string.stepscontent)));
		
		
	}

		
	
	
	/** Called when the user touches the button */
	public void WatchVideo_OnClick(View view) {
		Intent intentmain = new Intent(this, Video.class);
    	startActivity(intentmain);
		/*VideoView video0=(VideoView)findViewById(R.id.VideoView);
		video0.setMediaController(new MediaController(this));
		video0.setVideoURI(Uri.parse("android.resource://" +getPackageName()+ "/" +R.raw.igmr));
		video0.requestFocus();
		video0.start();*/
		/*MediaPlayer mediaPlayer =  MediaPlayer.create(getBaseContext(), R.raw.video);
		try{
		
	    mediaPlayer.start(); 
		}
		catch(Exception e){
			Log.e("Video",e.getMessage());
		}	*/
		
	}
	
	/** Called when the user touches the button */
	public void Main_OnClick(View view) {
	    // Do something in response to button click
		Intent intentmain = new Intent(this, Main.class);
    	startActivity(intentmain);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.
		
	}
	
	

	

	

}
